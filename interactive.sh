#!/usr/bin/env bash
while inotifywait -e modify "$1.idr"; do "$(dirname "$0")/pretty.py" "$1"; done
