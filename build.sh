#!/usr/bin/env bash
set -e
IDRIS2="idris2 -p idris2 -p contrib -p network"
SRC="IdrisJSON"
$IDRIS2 "$SRC/Generator.idr" -o generate
./build/exec/generate Idris2
SCHEMA=schema.json
PRETTY_SCHEMA=$(mktemp)
jq . "$SCHEMA" > "$PRETTY_SCHEMA"
cp "$PRETTY_SCHEMA" "$SCHEMA"
$IDRIS2 "$SRC/Main.idr" -o idris2-parser
