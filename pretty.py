#!/usr/bin/env python3

"""
Parse an Idris 2 file named "$1.idr" into a Python data file "$1.py".

Assumes that the Idris 2 parser is built in build/exec relative to this
script.
"""

import json
import os
import pathlib
import pprint
import subprocess
import sys

script_dir = os.path.dirname(sys.argv[0])
filename = sys.argv[1]
# use jq to remove decimal points from integers, just like in build.sh
cmd = f'"{script_dir}/build/exec/idris2-parser" "{filename}.idr" | jq -c .'
parsed = subprocess.check_output(cmd, encoding='utf-8', shell=True)
doubly_parsed = json.loads(parsed)
with open(f'{filename}.py', 'w') as file:
    pprint.pprint(doubly_parsed, stream=file)
