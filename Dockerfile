FROM ubuntu:18.04

# install Git, Make, GCC, Chez Scheme to clone and build Idris 2
RUN apt-get update
RUN apt-get -y install git
RUN apt-get -y install make
RUN apt-get -y install gcc
RUN apt-get -y install chezscheme

# download Idris 2
ARG IDRIS2_COMMIT
RUN git clone -n https://github.com/idris-lang/Idris2.git
WORKDIR /Idris2
RUN git checkout $IDRIS2_COMMIT

# install Idris 2
RUN make bootstrap SCHEME=chezscheme9.5
RUN make install
ENV PATH "/root/.idris2/bin:$PATH"

# self-host Idris 2
RUN make clean
RUN make all
RUN make install

# install Idris 2 API
RUN make install-api

# go back to the default working directory
WORKDIR /

# install jq to use in the build script
RUN apt-get -y install jq

# install other dependencies, needed for the tests
RUN apt-get -y install python3-pip
COPY requirements.txt .
RUN pip3 install -r requirements.txt
