||| Entry point to generate two files:
||| - `IdrisJSON/SyntaxJSON.idr` with `ToJSON` implementations for syntax types
||| - `schema.json` with a JSON Schema describing the output of `toJSON` for the
|||   `Idris.Syntax.Module` type
|||
||| For the former, many of the code generation functions here are not
||| parametric in the level of indentation; that is, each function assumes an
||| _n_-space indentation context, where _n_ is specific to that function but
||| fixed for all calls to that function. If such a function returns a multiline
||| string, the first line will not be indented, but each following line will be
||| indented at least _n_ spaces (usually more, to try to follow Idris
||| formatting conventions).
module Main

import Data.List
import Data.SortedMap
import Data.SortedSet
import Data.String.Extra
import Data.Strings
import Data.Vect

import Idris.Syntax

import IdrisJSON.Parser

import Language.JSON.Data

import System
import System.File

||| The dot-separated components of a module from `Idris2/src`.
ModuleName : Type
ModuleName = List String

||| Return the name of a `data` declaration.
dataDeclName : PDataDecl -> String
dataDeclName (MkPData _ name _ _ _) = show name
dataDeclName (MkPLater _ name _) = show name

||| Return the name of a declaration, if it is a `record` or `data` declaration.
declName : PDecl -> Maybe String
declName (PRecord _ _ _ name _ _ _) = Just (show name)
declName (PData _ _ _ dataDecl) = Just (dataDeclName dataDecl)
declName _ = Nothing

||| Return generated code for the definition of a `toJSON` case.
||| Assumes a four-space indentation context.
||| @ lhs the pattern to match for this `toJSON` case to apply
||| @ rhs the body of this `toJSON` case
deriveCase : (lhs : String) -> (rhs : String) -> String
deriveCase lhs rhs = "toJSON " ++ lhs ++ "\n      = " ++ rhs

||| Return generated code implementing `ToJSON` for a type.
||| Assumes a two-space indentation context.
||| @ name the fully-qualified name of the type to implement `ToJSON` for
||| @ cases a list of (lhs, rhs) pairs to be passed to `deriveCase`
derive : (name : String) -> (cases : List (String, String)) -> String
derive name cases
  = join "\n    " (("  export\n  ToJSON " ++ name ++ " where")
                   :: map (uncurry deriveCase) cases)

||| Return `show y` if `x` is `Just y`, otherwise the empty string.
showMaybe : Show ty => (x : Maybe ty) -> String
showMaybe = fromMaybe "" . map show

||| Return the name of a field.
fieldName : PField -> String
fieldName (MkField _ _ _ _ name _) = show name

||| Return code for an expression constructing `JObject` mapping for a field.
||| The expression constructs a pair whose first element is the name of the
||| field and whose second element converts the value of the field to JSON.
||| Assumes that the expression will be used in a context where the value of the
||| field is bound via a local variable with the same name as the field.
fieldPair : PField -> String
fieldPair field
  = let name = (fieldName field) in
    "(\"" ++ name ++ "\", toJSON " ++ name ++ ")"

||| Return the list of arguments in a function type.
||| For each element of the returned list, if that argument was given an
||| explicit name in the function type, then that name is in the `fst` element
||| of the pair. In any case, the type of the argument is in the `snd` element.
args : PTerm -> List (Maybe Name, PTerm)
args (PPi _ _ _ name argTy retTy) = (name, argTy) :: args retTy
args _ = []

||| Return a list of possible argument name to use when defining a function.
||| Any names in the type are used; other arguments are named `x1`, `x2`, etc.
||| @ ty the type of the function to construct an argument list for
funcParams : (ty : PTerm) -> List String
funcParams term
  = let (_, params) = foldl f (1, []) (args term) in reverse params
    where f : (Nat, List String) -> (Maybe Name, PTerm) -> (Nat, List String)
          f (n, params) (_, (PBracketed _ _)) = (n, "_" :: params)
          f (n, params) (Just name, _) = (n, show name :: params)
          f (n, params) _ = (n + 1, ("x" ++ show n) :: params)

||| Separator for variable names in a `toJSON` pattern-match.
||| The spaces are chosen to be the same length as "    toJSON (".
patternJoin : String
patternJoin = "\n            "

||| Separator for list items in a `JArray` right-hand side.
||| The spaces are chosen to be the same length as "      = JArray [".
paramJoin : String
paramJoin = ",\n                "

||| Return a (lhs, rhs) pair of code for `toJSON` of a `data` constructor.
||| To be used in `deriveCase`.
||| @ tyDecl the type declaration of the `data` constructor
constructorCase : (tyDecl : PTypeDecl) -> (String, String)
constructorCase (MkPTy _ name _ ty)
  = let name = show name in
    let params = funcParams ty in
    let pattern = join patternJoin (name :: params) in
    let jsonName = "JString \"" ++ name ++ "\"" in
    let jsonify = \name => if name == "_" then "JNull" else "toJSON " ++ name in
    let jsonParams = map jsonify params in
    (if isNil params then pattern else "(" ++ pattern ++ ")",
     "JArray [" ++ join paramJoin (jsonName :: jsonParams) ++ "]")

||| Return a list of (lhs, rhs) pairs for `toJSON` of a `data` declaration.
dataCases : PDataDecl -> List (String, String)
dataCases (MkPData _ _ _ _ constructors) = map constructorCase constructors
dataCases _ = []

||| Separator for key-value pairs in a `JObject` right-hand side.
||| The spaces are chosen to be the same length as "      = JObject [".
fieldJoin : String
fieldJoin = ",\n                 "

||| Return generated code implementing `ToJSON` for a `data` or `record` type.
||| Assumes a two-space indentation context.
||| Uses the given `modName` to fully-qualify the name of the type.
||| @ modName the name of the module in which the type resides.
||| @ decl the declaration of the type to implement `ToJSON` for
deriveDecl : (modName : ModuleName) -> (decl : PDecl) -> String
deriveDecl modName (PRecord _ _ _ name _ cname fields)
  = let pattern = showMaybe cname :: (map fieldName fields) in
    derive (join "." (modName ++ [show name]))
           [("(" ++ join patternJoin pattern ++ ")",
             "JObject [" ++ join fieldJoin (map fieldPair fields) ++ "]")]
deriveDecl modName (PData _ _ _ dataDecl)
  = derive (join "." (modName ++ [dataDeclName dataDecl])) (dataCases dataDecl)
deriveDecl _ _ = ""

||| Return each declaration that is either top-level or in a `mutual` block.
expandMutuals : List PDecl -> List PDecl
expandMutuals
  = concat . map expandMutual
    where expandMutual : PDecl -> List PDecl
          expandMutual (PMutual _ decls) = decls
          expandMutual decl = [decl]

||| Return a map from type names to `data` and `record` types from the module.
||| The keys of the map are _unqualified_ type names.
moduleDecls : Module -> SortedMap String PDecl
moduleDecls (MkModule _ _ _ _ decls)
  = foldl (\m, (n, d) => insert n d m)
          empty
          (mapMaybe (\d => map (\n => (n, d)) (declName d))
                    (expandMutuals decls))

||| A map from unqualified type names to pairs:
||| - `fst` gives the module in which the type was declared.
||| - `snd` gives the declaration of the type.
AnnotatedMap : Type
AnnotatedMap = SortedMap String (ModuleName, PDecl)

||| Return an annotated version of `m` assuming each type comes from `mname`.
annotate : (mname : ModuleName) -> (m : SortedMap String PDecl) -> AnnotatedMap
annotate mname m
  = fromList (map f (toList m))
    where f : (String, PDecl) -> (String, (ModuleName, PDecl))
          f (dname, d) = (dname, (mname, d))

||| Return a map of all the `data` and `record` types from all given modules.
||| @ modules a map from some set of module names to the modules named by them
declMap : (modules : SortedMap ModuleName Module) -> AnnotatedMap
declMap modules
  = foldl mergeLeft empty (map f (Data.SortedMap.toList modules))
    where f : (ModuleName, Module) -> AnnotatedMap
          f (name, mod) = annotate name (moduleDecls mod)

||| Return a set of unqualified names of all non-primitive types used in `ty`.
||| @ ty an expression representing a type.
tyDeps : (ty : PTerm) -> SortedSet String
tyDeps (PRef _ name) = fromList [show name]
tyDeps (PPi _ _ _ _ argTy retTy) = union (tyDeps argTy) (tyDeps retTy)
tyDeps (PApp _ f x) = union (tyDeps f) (tyDeps x)
tyDeps _ = empty

||| Return unqualified names of all non-primitive types needed by `decl`.
||| @ decl a `record` or `data` declaration
dependencies : (decl : PDecl) -> SortedSet String
dependencies decl
  = let sets = case decl of
                    PRecord _ _ _ _ _ _ fields =>
                      map (\(MkField _ _ _ _ _ ty) => tyDeps ty) fields
                    PData _ _ _ (MkPData _ _ _ _ constructors) =>
                      map (\(MkPTy _ _ _ ty) => tyDeps ty) constructors
                    _ => []
    in foldl union empty sets

||| Return all types from `decls` used recursively by the type named by `start`.
||| Performs a graph search in `decls` where out-edges are given by
||| `dependencies`. Only considers `record` and `data` types. Ignores the
||| `Core.TT.PiInfo` type because it is specially handled by `IdrisJSON.ToJSON`.
||| @ decls the type declarations in which to search
||| @ start the unqualified name of the type from which to start the search
search : (decls : AnnotatedMap) -> (start : String) -> List (ModuleName, PDecl)
search decls start
  = values (f (mapMaybe (\s => lookup s decls) [start]) empty)
    where f : List (ModuleName, PDecl) -> AnnotatedMap -> AnnotatedMap
          f [] done = done
          f (current :: todo) sofar
            = let currentName = fromMaybe "" (declName (snd current)) in
              case lookup currentName decls of
                   Nothing => f todo sofar
                   Just (modName, decl) =>
                     let sofar = insert currentName (modName, decl) sofar in
                     let toList = Data.SortedSet.toList in
                     let newOnes
                       = toList (difference (dependencies decl)
                                            (insert "PiInfo" (keySet sofar)))
                       in
                     f (todo ++ mapMaybe (\s => lookup s decls) newOnes) sofar

||| Return the concatenation of generated `ToJSON` code for a list of types.
||| @ decls a list of type declarations, with the modules from which they came
deriveAll : (decls : List (ModuleName, PDecl)) -> String
deriveAll pairs = join "\n\n" (map (uncurry deriveDecl) pairs)

||| Return the filename of a `src` module from the Idris 2 source code.
||| @ idris the location of the Idris 2 source code, with no trailing slash
expandFilename : (idris : String) -> ModuleName -> String
expandFilename idris moduleParts
  = join "/" (idris :: "src" :: moduleParts) ++ ".idr"

||| Attempt to parse a list of `src` modules from the Idris 2 source code.
||| Return `Left` with an error message, or `Right` if successful.
||| @ idris the location of the Idris 2 source code, with no trailing slash
parseAll : (idris : String) -> List ModuleName ->
           IO (Either String (SortedMap ModuleName Module))
parseAll _ [] = pure (Right empty)
parseAll idris (file :: files)
  = do Right rest <- parseAll idris files
         | Left err => pure (Left err)
       case lookup file rest of
            Just _ => pure (Right rest)
            Nothing => do
              let full = expandFilename idris file
              Right parsed <- parse full
                | Left err => pure (Left ("Error in " ++ full ++ ": " ++ err))
              pure (Right (insert file parsed rest))

||| Add the modules needed for `ToJSON` code that don't contain syntax types.
augment : List ModuleName -> List ModuleName
augment mods = ["IdrisJSON", "ToJSON"] :: ["Language", "JSON", "Data"] :: mods

||| Return a map from first elements to remaining elements.
||| The key of each mapping is the `head` of a list from the input, and
||| the value of that mapping is the set containing the `tail` of each list from
||| the input that had that key as its `head`.
||| All empty lists from the input are discarded.
byHead : Ord t => List (List t) -> SortedMap t (SortedSet (List t))
byHead
  = foldl (\m, l => case l of
                         [] => m
                         (x :: xs) => let s = fromMaybe empty (lookup x m) in
                                      insert x (insert xs s) m)
          empty

||| Return code to import a list of modules with the same first component.
||| Assumes a zero-space indentation context.
||| Ends with a trailing newline.
||| @ hd the first component of all the modules to import in this block
||| @ tls all the modules to import, each with its first component removed
importBlock : (hd : String) -> (tls : SortedSet ModuleName) -> String
importBlock hd tls = unlines (map (\tl => "import " ++ (join "." (hd :: tl)))
                                  (foldr (::) [] tls))

||| Return code to `import` all the given modules.
||| Assumes a zero-space indentation context.
||| Ends with a trailing newline.
||| The `import`s are grouped by the first component of the module being
||| imported. The groups are separated by blank lines.
makeImports : List ModuleName -> String
makeImports = let toList = Data.SortedMap.toList in
              join "\n" . map (uncurry importBlock) . toList . byHead . augment

||| Return a JSON Schema that matches things of the given primitive type.
||| @ ty the name of a primitive JSON Schema type
primSchema : (ty : String) -> JSON
primSchema ty = JObject [("type", JString ty)]

||| A JSON Schema that matches booleans.
boolSchema : JSON
boolSchema = primSchema "boolean"

||| A JSON Schema that matches integers.
intSchema : JSON
intSchema = primSchema "integer"

||| A JSON Schema that matches integers and floating point numbers.
numSchema : JSON
numSchema = primSchema "number"

||| A JSON Schema that matches null.
nullSchema : JSON
nullSchema = primSchema "null"

||| A JSON Schema that matches strings.
strSchema : JSON
strSchema = primSchema "string"

||| A JSON Schema that matches natural numbers.
natSchema : JSON
natSchema = JObject [("type", JString "integer"), ("minimum", JNumber 0)]

||| Return a JSON Schema that matches either null or things matching `inner`.
||| Assumes that `inner` cannot match null.
||| @ inner a JSON Schema
maybeSchema : (inner : JSON) -> JSON
maybeSchema inner = JObject [("oneOf", JArray [nullSchema, inner])]

||| Return a JSON Schema that matches lists of elements matching `elem`.
||| @ elem the JSON Schema that the list elements must match
listSchema : (elem : JSON) -> JSON
listSchema elem = JObject [("type", JString "array"), ("items", elem)]

||| Return a JSON Schema that matches arrays of two elements.
||| Also allows arrays longer than two elements, checking only those first two.
||| @ left the JSON Schema that the first element must match
||| @ right the JSON Schema that the second element must match
pairSchema : (left : JSON) -> (right : JSON) -> JSON
pairSchema left right
  = JObject [("type", JString "array"),
             ("minItems", JNumber 2),
             ("items", JArray [left, right])]

||| Return a JSON Schema that matches the schema defined for a given type.
||| Assumes that `definitions` contains a schema named `typeName`.
||| @ typeName the unqualified name of a type
refSchema : (typeName : String) -> JSON
refSchema typeName = JObject [("$ref", JString ("#/definitions/" ++ typeName))]

||| Return `schema` with the given `value` for the given `keyword` adjoined.
||| Returns `schema` unchanged if it is not a JSON object.
withKeyword : (keyword : String) -> (value : String) -> (schema : JSON) -> JSON
withKeyword keyword value (JObject fields)
  = JObject ((keyword, JString value) :: fields)
withKeyword _ _ schema = schema

||| Return `schema` with `title` as its "title" keyword.
||| Returns `schema` unchanged if it is not a JSON object.
withTitle : (title : String) -> (schema : JSON) -> JSON
withTitle = withKeyword "title"

||| Return a JSON Schema that matches things of the given type.
||| Assumes that data and record types are present (by name) in `definitions`.
||| Returns null (not `nullSchema`) iff unsure how to generate a schema.
||| @ ty an expression representing a type
tySchema : (ty : PTerm) -> JSON
tySchema (PRef _ name)
  = let name = show name in
    case name of
         "Bool" => boolSchema
         "FileName" => withTitle name strSchema
         "FilePos" => withTitle name (pairSchema intSchema intSchema)
         "Nat" => natSchema
         "OpStr" => withTitle name (refSchema "Name")
         _ => refSchema name
tySchema (PPi _ _ _ _ _ _) = nullSchema
tySchema (PApp _ (PRef _ name) x)
  = case show name of
         "List" => listSchema (tySchema x)
         "Maybe" => maybeSchema (tySchema x)
         fName =>
           case x of
                PRef _ xName => refSchema (fName ++ "_" ++ show xName)
                _ => JNull
tySchema (PPrimVal _ IntType) = intSchema
tySchema (PPrimVal _ IntegerType) = withTitle "Integer" intSchema
tySchema (PPrimVal _ StringType) = strSchema
tySchema (PPrimVal _ CharType) = withTitle "Char" strSchema
tySchema (PPrimVal _ DoubleType) = numSchema
tySchema (PBracketed _ term) = tySchema term
tySchema (PPair _ left right) = pairSchema (tySchema left) (tySchema right)
tySchema _ = JNull

||| Return `schema` with `comment` as its "$comment" keyword.
||| Returns `schema` unchanged if it is not a JSON object.
withComment : (comment : String) -> (schema : JSON) -> JSON
withComment = withKeyword "$comment"

||| Return the name and a list of schemas for the arguments of a function type.
||| So named because it is intended to be used for `data` constructors.
||| @ tyDecl the type declaration of a `data` constructor
consInfo : (tyDecl : PTypeDecl) -> (String, List JSON)
consInfo (MkPTy _ name _ ty)
  = (show name, map (uncurry f . map tySchema) (args ty))
    where f : Maybe Name -> JSON -> JSON
          f (Just name) = withComment (show name)
          f Nothing = id

||| Return a JSON Schema for a `data` constructor.
||| @ name the name of the `data` constructor
||| @ params a JSON Schema for each parameter of the `data` constructor
consSchema : (name : String) -> (params : List JSON) -> JSON
consSchema name params
  = let minItems = JNumber (cast (1 + length params)) in
    let items = JArray (JObject [("const", JString name)] :: params) in
    JObject [("type", JString "array"),
             ("minItems", minItems),
             ("items", items)]

||| Return a JSON Schema for a `data` type.
||| Each element of `constructors` consists of the name of a constructor and a
||| list of JSON Schemas, one for each parameter of that constructor.
dataSchema : (constructors : List (String, List JSON)) -> JSON
dataSchema constructors
  = JObject [("oneOf", JArray (map (uncurry consSchema) constructors))]

||| Return the name of, and a JSON Schema for, a field of a `record` type.
fieldProperty : PField -> (String, JSON)
fieldProperty field
  = (fieldName field, let MkField _ _ _ _ _ ty = field in tySchema ty)

||| Return a JSON Schema for the type given by a `data` or `record` declaration.
declSchema : PDecl -> JSON
declSchema (PData _ _ _ (MkPData _ name _ _ constructors))
  = dataSchema (map consInfo constructors)
declSchema (PRecord _ _ _ _ _ _ fields)
  = JObject [("type", JString "object"),
             ("required", JArray (map (JString . fieldName) fields)),
             ("properties", JObject (map fieldProperty fields))]
declSchema _ = JNull

||| Return a (name, schema) pair for use in the `definitions` of a JSON Schema.
||| @ decl a `data` or `record` declaration for the type to define the schema of
schemaDef : (decl : PDecl) -> Maybe (String, JSON)
schemaDef decl = map (\name => (name, declSchema decl)) (declName decl)

||| Return a (name, schema) pair for a specific version of a parametric type.
||| @ f the name of the generic type
||| @ g a function giving a schema for any specific value of the type parameter
||| @ x the name of the specific type serving as the value of the type parameter
paramDef : (f : String) -> (g : String -> JSON) ->
           (x : String) -> (String, JSON)
paramDef f g x = (f ++ "_" ++ x, g x)

||| A list of necessary but not automatically-generated (name, schema) pairs.
extraDefs : List (String, JSON)
extraDefs
  = let piInfo = paramDef "PiInfo"
                          (\x => dataSchema [("Implicit", []),
                                             ("Explicit", []),
                                             ("AutoImplicit", []),
                                             ("DefImplicit", [refSchema x])]) in
    [("RigCount", dataSchema [("Rig0", []), ("Rig1", []), ("RigW", [])]),
     piInfo "PTerm",
     piInfo "RawImp"]

||| Return a JSON Schema for `ToJSON Idris.Syntax.Module`.
||| @ decls a list of `data` and `record` type declarations returned by `search`
schema : (decls : List PDecl) -> JSON
schema decls
  = JObject [("$schema", JString "http://json-schema.org/draft-07/schema#"),
             ("anyOf", JArray [refSchema "Module"]),
             ("definitions", JObject (extraDefs ++ (mapMaybe schemaDef decls)))]

||| All the modules containing types for which we need `ToJSON` implementations.
importNames : List ModuleName
importNames
  = [["Core", "Core"],
     ["Core", "FC"],
     ["Core", "Name"],
     ["Core", "Options"],
     ["Core", "TT"],
     ["Idris", "Syntax"],
     ["TTImp", "TTImp"]]

||| Write generated code to `IdrisJSON/SyntaxJSON.idr` for an Idris 2 module
||| with `ToJSON` implementations of all the types needed to convert an
||| `Idris.Syntax.Module` object to JSON.
||| Also write a JSON Schema to `schema.json` describing the output of that
||| conversion.
||| Assumes that the only command-line argument is the directory name of a local
||| clone of the Idris 2 source code, with a compatible commit checked out.
main : IO ()
main = do [_, idris] <- getArgs
            | _ => putStrLn "Please specify one argument: the Idris location."
          let imports = makeImports importNames
          Right modules <- parseAll idris importNames
            | Left err => putStrLn err
          let decls = search (declMap modules) "Module"
          let derivations = deriveAll decls
          let source = unlines ["module IdrisJSON.SyntaxJSON\n", imports,
                                "mutual", derivations]
          Right _ <- writeFile "IdrisJSON/SyntaxJSON.idr" source
            | Left err => putStrLn (show err)
          let schemaText = show (schema (map snd decls)) ++ "\n"
          Right _ <- writeFile "schema.json" schemaText
            | Left err => putStrLn (show err)
          pure ()
