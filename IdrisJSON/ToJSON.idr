||| A `ToJSON` interface for types that can be converted to JSON.
||| Includes implementations for several basic types, plus these two:
||| - `Algebra.RigCount`, because its constructors are private so it can't use
|||   the strategy that the code generator uses
||| - `Core.TT.PiInfo`, because it takes a type parameter which the code
|||   generator can't handle
|||
||| The intended strategy is as follows:
||| - primitive types are converted directly to their JSON equivalents
||| - `null` arises either from the `Nothing` of a `Maybe` or from something
|||   that can't be converted, such as a function
||| - lists and tuples are converted to arrays
||| - `record` types are converted to objects
||| - `data` types are converted to arrays where the first element is the name
|||   of the constructor
module IdrisJSON.ToJSON

import Algebra

import Core.FC
import Core.TT

import Data.Strings

import Language.JSON.Data

||| An object that can be converted to JSON.
public export
interface ToJSON t where
  ||| Return a JSON representation of an object.
  ||| @ x the object to convert to JSON.
  toJSON : (x : t) -> JSON

export
ToJSON t => ToJSON (Maybe t) where
  toJSON Nothing = JNull
  toJSON (Just x) = toJSON x -- could cause ambiguity in directly-nested Maybes

export
ToJSON Bool where
  toJSON = JBoolean

export
ToJSON Double where
  toJSON = JNumber

export
ToJSON Integer where
  toJSON = JNumber . cast

export
ToJSON Int where
  toJSON = JNumber . cast

export
ToJSON Nat where
  toJSON = toJSON . natToInteger

export
ToJSON String where
  toJSON = JString

export
ToJSON Char where
  toJSON = toJSON . singleton

export
ToJSON t => ToJSON (List t) where
  toJSON list = JArray (map toJSON list)

export
(ToJSON t1, ToJSON t2) => ToJSON (t1, t2) where
  toJSON (x1, x2) = JArray [toJSON x1, toJSON x2]

export
ToJSON RigCount where
  toJSON rc = JArray [JString (show rc)]

export
ToJSON t => ToJSON (PiInfo t) where
  toJSON Implicit = JArray [JString "Implicit"]
  toJSON Explicit = JArray [JString "Explicit"]
  toJSON AutoImplicit = JArray [JString "AutoImplicit"]
  toJSON (DefImplicit x) = JArray [JString "DefImplicit", toJSON x]
