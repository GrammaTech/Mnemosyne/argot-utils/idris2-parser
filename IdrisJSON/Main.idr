||| Entry point to the command-line Idris 2 parser, which outputs JSON.
module Main

import Idris.Syntax

import IdrisJSON.Parser
import IdrisJSON.SyntaxJSON
import IdrisJSON.ToJSON

import Language.JSON.Data

import System

||| Print to stdout the parse tree of an Idris 2 file specified by argument.
main : IO ()
main = do [_, file] <- getArgs
            | _ => putStrLn "Please specify one argument: a filename."
          Right parsed <- parse file
            | Left err => putStrLn err -- should put this in stderr
          putStrLn (show (toJSON parsed))
