||| Utility namespace for invoking the Idris 2 parser programmatically.
module IdrisJSON.Parser

import Idris.Parser
import Idris.Syntax

import Parser.Source

import System.File

||| Attempt to read and parse a file containing Idris 2 code.
||| Return `Left` with an error message, or `Right` if successful.
||| @ filename the name of the file to read and parse
export
parse : (filename : String) -> IO (Either String Module)
parse file
  = do Right code <- readFile file
         | Left err => pure (Left (show err))
       Right parsed <- pure (runParser Nothing code
                                       (do p <- prog file; eoi; pure p))
         | Left err => pure (Left (show err))
       pure (Right parsed)
