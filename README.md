# Idris 2 parser

Idris 2 program to parse Idris 2 code using the [Idris 2 API][custom backends]
and then print the AST as JSON.

## Usage

This repository contains the [idris-lang/Idris2][] GitHub repository as a Git
submodule, so be sure to clone it using `--recurse-submodules`. If you don't
already have Idris 2 installed, first follow the Idris 2 install instructions in
`Idris2/INSTALL.md`, including step 5 ("Installing the Idris 2 API").

### Parser

Also be sure to install [jq][], as it is needed in the build script. Then after
installing all dependencies, build this project using `build.sh`:
```
$ ./build.sh
```
The built parser should be in `build/exec/idris2-parser`:
```
$ ./build/exec/idris2-parser hello.idr > hello.json
```

### Schema

The build should also have generated a [JSON Schema][] in `schema.json`, which
you can use to validate the output via a [JSON Schema implementation][]:
```
$ ajv -s schema.json -d hello.json
hello.json valid
```
The schema itself can also be used to generate code to define data types for
Idris 2 syntax in other languages.

### Interactive

To get a feel for how Idris 2 source code translates to a parsed AST, it can be
useful to see the two side-by-side in sync while editing. On the one hand, the
default output of the parser is compact (similar to `jq -c .`) and thus
difficult to read, but on the other hand, most JSON pretty-printers (such as `jq
.`) go too far in the other direction, spreading out the AST so much that, for
instance, a `MkFC` node containing a location in source code (filename, start
position, and end position) can take up to 12 lines. The `pretty.py` script in
this repository uses the Python 3 pretty-printer to provide a nice balance
between these two extremes. As an example, if you're on Linux and have `python3`
and `inotify-tools` installed, you can get that pretty-printed parsed AST every
time you edit a file:
```
$ cd IdrisJSON
$ ../interactive.sh Generator
```
As shown in the above example, the provided script should be able to be run from
any directory; just give it the name of an Idris 2 file minus the `.idr` part.
Then if you use VS Code, for instance, you can open up
`IdrisJSON/{Generator.idr,Generator.py}` side-by-side and save the former each
time you want to see the corresponding change to the latter.

## Tests

The tests use the built artifact of this project to parse its own source code
and validate that parsed output against the generated JSON Schema. To run the
tests, first install Python 3 and Pip, as well as the specific Python modules
required for the test script:
```
$ pip3 install -r requirements.txt
```
Then run the tests:
```
$ ./test.py
```
If the tests pass, this should print no output and return an error code of zero;
otherwise, neither of those things should be true.

## Docker image

The top-level `Dockerfile` in this repository specifies an installation of Idris
2 (as well as other dependencies for the test script) in Ubuntu 18.04. When
building it, be sure to pass the commit hash of the Idris 2 Git submodule in
this repository:
```
$ TAG=registry.gitlab.com/grammatech/mnemosyne/argot-utils/idris2-parser
$ docker build --build-arg IDRIS2_COMMIT="$(./idris2_commit.sh)" -t "$TAG" .
$ docker push "$TAG"
```

[custom backends]: https://idris2.readthedocs.io/en/latest/backends/custom.html
[idris-lang/Idris2]: https://github.com/idris-lang/Idris2
[jq]: https://stedolan.github.io/jq/
[json schema]: https://json-schema.org/
[json schema implementation]: https://json-schema.org/implementations.html
