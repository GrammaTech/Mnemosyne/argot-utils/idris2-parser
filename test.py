#!/usr/bin/env python3

"""
Check that all source files in this project parse and validate.

First uses a third-party JSON Schema validator to validate the generated
schema against its meta-schema. Then iterates through all the Idris 2
source files (including any generated code) in the source directory,
parsing each one using the built artifact, and validating the parsed
output against the generated schema.

If passed a filename as an argument, checks that that file is identical
to schema.json before running the tests.
"""

import filecmp
import json
import os
import subprocess
import sys

import fastjsonschema
import jsonschema

schema_file = 'schema.json'

# if passed an argument, compare schema against that file
if len(sys.argv) > 1:
    other_file = sys.argv[1]
    if not filecmp.cmp(schema_file, other_file):
        sys.exit(f'{schema_file} differs from {other_file}')

# fastjsonschema needs jq to remove decimal points from integers
with open(schema_file) as file:
    schema = json.load(file)

# fastjsonschema doesn't explicitly support meta-validation
jsonschema.Draft7Validator.check_schema(schema)

# jsonschema chokes on the actual validation
validate = fastjsonschema.compile(schema)
src = 'IdrisJSON'
for filename in os.listdir(src):
    if filename.endswith('.idr'):
        args = ['./build/exec/idris2-parser', os.path.join(src, filename)]
        data = subprocess.check_output(args, encoding='utf-8')
        validate(json.loads(data))
